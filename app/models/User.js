module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      allowNull: false,
      primaryKey: true
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    superUser: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
  });
  return User;
};
