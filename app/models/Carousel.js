module.exports = (sequelize, Sequelize) => {
    const Carousel = sequelize.define("carousel", {
      email: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      folder: {
          type: Sequelize.STRING,
          allowNull: false
      },
      description: {
          type: Sequelize.STRING,
          allowNull: false
      },
      title: {
          type: Sequelize.STRING,
          allowNull: false,
      }
    });
    return Carousel;
  };
  