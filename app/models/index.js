const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
var User = require("./User.js")(sequelize, Sequelize);
var Carousel = require("./Carousel.js")(sequelize, Sequelize);

// Here we can connect countries and cities base on country code
User.hasMany(Carousel, {foreignKey: 'email', sourceKey: 'email'});
Carousel.belongsTo(User, {foreignKey: 'email', targetKey: 'email'});

db.user = User
db.carousel = Carousel

module.exports = db;
