const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

// Load User model
var db = require("../models");
var User = db.user;
const Op = db.Sequelize.Op;


module.exports = function(passport) {
  passport.use(
    new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
      User.findOne({
        where: { email: email }
      }).then(user => {
        if (!user) {
          return done(null, false, { message: 'That email is not registered' });
        }
        bcrypt.compare(password, user.password, (err, isMatch) => {
          if (err) throw err;
          if (isMatch) {
            return done(null, user);
          } else {
            return done(null, false, { message: 'Password incorrect' });
          }
        });
      });
    })
  );
  passport.serializeUser(function(user, done) {
    done(null, user.email);
    // if you use Model.id as your idAttribute maybe you'd want
    // done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    console.log(id)
    User.findByPk(id)
    .then(user =>{
      return done(null,user)
    })
    .catch(err => {
      return done(err,null)
    });
  });
};
