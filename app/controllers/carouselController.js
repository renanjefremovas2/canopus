const db = require("../models");
const Carousel = db.carousel;
const Op = db.Sequelize.Op;
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');


exports.create = (req, res) => {
  var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      if(!fields.title || !fields.description || !files){
        req.flash('error_msg', 'A field is missing');
        res.redirect('/dashboard')
        return
      }

      appRoot = require('path').resolve('./app/')
      const carousel = {
        email: req.user.dataValues.email,
        folder: "/images/" + files.file.name,
        description: fields.description,
        title: fields.title
      };
      Carousel.create(carousel)
        .then(data => {
          req.flash('success_msg', 'Carousel created with success');
          res.render('../views/dashboard.ejs', {
            user: req.user,
            data: null,
            success_msg: "Carousel created with success"
          })
        })
        .catch(err => {
          req.flash('error_msg', 'Error creating carousel');
          res.redirect('/dashboard')
          return
        });
      var oldpath = files.file.path;
      var newpath = appRoot + "/public" + carousel.folder;
      fs.rename(oldpath, newpath, function (err) {
        if (err) {
          req.flash('error_msg', 'Error renaming carousel');
          res.redirect('/dashboard')
          return
        } else{
          req.flash('success_msg', 'Carousel created with success');
          res.redirect('/dashboard')
          return
        }
      });
    });
  
};

// Retrieve all Carousels from the database.
exports.findAll = (req, res) => {
  Carousel.findAll({ where: 
    {
      email: req.user.email
    } 
  })
    .then(data => {
      res.render('../views/dashboard.ejs',
      {
          user: req.user,
          data: data,
      });
    })
    .catch(err => {
      res.render('../views/dashboard.ejs',
          { 
            data: null,
            user: req.user,
            error: "Error finding carousel",
          });
    });
};


// Delete a Carousel with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Carousel.destroy({
    where: { 
        id: id,
    }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Carousel was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Carousel id=${id}. Maybe Carousel was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Carousel id=" + id
      });
    });
};

exports.deleteAll = (req, res) => {
    const email = req.params.email;
  
    Carousel.destroy({
      where: { 
        email: email,
      }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Carousel was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Carousel email=${email}. Maybe Carousel was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Carousel email=" + email
        });
      });
  };


