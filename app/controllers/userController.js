const db = require("../models");
const User = db.user;
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt')

exports.create = (req, res) => {
  if (!req.body.email || !req.body.password || !req.body.password) {
    req.flash('error', 'Content cannot be empty');
    res.redirect('/user/create')
    return
  }

  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(req.body.password, salt);
  if(!req.body.superUser){
    req.body.superUser = 0;
  }
  const user = {
    name: req.body.name,
    email: req.body.email,
    password: hash,
    superUser: req.body.superUser
  };

  User.create(user)
    .then(data => {
      req.flash('success_msg', 'User created, please log in');
      res.redirect('/user/create')
      return
    return;
    })
    .catch(err => {
      req.flash('error', 'Error creating user');
      res.redirect('/user/create')
      return
    });
};

exports.findAll = (req, res) => {

  User.findAll({ 
    where: {
      superUser : -1
    } 
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.render('../views/dashboard.ejs',
      {
        error_msg: "Couldnt retrieve data"
      });
      return;
    });
};

// exports.findOne = (req, res) => {
//   const email = req.params.email;

//   User.findByPk(email)
//     .then(data => {
//       res
//     })
//     .catch(err => {
//       res.status(500).send({
//         message: "Error retrieving User email=" + email
//       });
//     });
// };


exports.delete = (req, res) => {
  const email = req.params.email;

  User.destroy({
    where: { email: email }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete User email=${email}. Maybe User was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete User email=" + email
      });
    });
};


