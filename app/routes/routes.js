module.exports = app => {
  const user = require("../controllers/userController.js");
  const carousel = require("../controllers/carouselController.js");
  const passport = require('passport');
  const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth');
  var router = require("express").Router();

  router.get('/dashboard', ensureAuthenticated, carousel.findAll);

  router.get("/user/login", forwardAuthenticated, (req, res) => res.render('../views/login.ejs'));
  
  router.post('/user/login', (req, res, next) => {
    passport.authenticate('local', {
      successRedirect: '/user/login',
      failureRedirect: '/user/login',
      failureFlash: true
    })(req, res, next);
  });
  
  router.get('/user/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/user/login');
  });

  router.post("/user/create", user.create);
  router.get("/user/create", (req, res) => res.render('../views/register.ejs'));

  router.get("/user/", forwardAuthenticated, user.findAll);
  
  router.delete("/:email", forwardAuthenticated, user.delete);

  router.post("/user/carousel/create", carousel.create);

  router.get("/user/carousel/:email", ensureAuthenticated , carousel.findAll);
  
  router.delete("/user/carousel:id", ensureAuthenticated, carousel.delete);

  router.delete("/user/carousel/:email", ensureAuthenticated, carousel.deleteAll);

  app.use('/', router);
};
